const mongoose = require('mongoose');

const Provider = mongoose.model('Provider');
const Client = mongoose.model('Client');

const createProvider = async (req, res) => {
  try {
    const {
      name,
    } = req.body;
    const isCreated = await Provider.findOne({ name });
    if (isCreated) {
      return res.status(208).send({ response: 'ok', message: 'Provider is already exist' });
    }
    const counter = await Provider.find({}).sort({ id: 'desc' }).limit(1);
    const newProvider = new Provider({
      name,
      id: counter.length ? counter[0].id + 1 : 0,
    });
    await newProvider.save();
    return res.status(200).send({ response: 'ok', payload: newProvider });
  } catch (err) {
    return res
      .status(500)
      .send({ response: 'error', message: err.message });
  }
};

const deleteProvider = async (req, res) => {
  try {
    const { _id } = req.query;
    await Client.updateMany({ providers: _id }, { $pull: { providers: _id } });
    const resultDelete = await Provider.deleteOne({ _id });
    return res.status(200).send({ response: 'ok', payload: resultDelete });
  } catch (err) {
    return res
      .status(500)
      .send({ response: 'error', message: err.message });
  }
};

const updateProvider = async (req, res) => {
  try {
    const { _id } = req.query;
    const arrProviders = await Provider.find({ name: req.body.name });
    if (arrProviders.length > 1
      || (arrProviders.length && arrProviders[0]._id.toString() !== _id.toString())) {
      return res.status(208).send({ response: 'ok', message: 'Provider already exist' });
    }
    const editedProvider = await Provider.updateOne({ _id }, { $set: req.body });
    return res.status(200).send({ response: 'ok', payload: editedProvider });
  } catch (err) {
    return res
      .status(500)
      .send({ response: 'error', message: err.message });
  }
};

const getProviders = async (req, res) => {
  try {
    const arrayProviders = await Provider.find({});
    return res.status(200).send({ response: 'ok', payload: arrayProviders });
  } catch (err) {
    return res
      .status(500)
      .send({ response: 'error', message: err.message });
  }
};

module.exports = {
  createProvider,
  deleteProvider,
  updateProvider,
  getProviders,
};
