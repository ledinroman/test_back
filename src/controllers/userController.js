const mongoose = require('mongoose');

const Client = mongoose.model('Client');
const Provider = mongoose.model('Provider');

const createClient = async (req, res) => {
  try {
    const {
      name, email, phone, providers,
    } = req.body;
    const isCreated = await Client.findOne({ email });
    if (isCreated) {
      return res.status(208).send({ response: 'ok', message: 'Email is already used' });
    }
    const client = new Client({
      name,
      email,
      phone,
      providers,
    });
    await client.save();
    return res.status(200).send({ response: 'ok', payload: client });
  } catch (err) {
    return res
      .status(500)
      .send({ response: 'error', message: err.message });
  }
};

const deleteClient = async (req, res) => {
  try {
    const { _id } = req.query;
    const checkExist = await Client.findOne({ _id });
    if (!checkExist) {
      return res.status(403).send({ response: 'error', message: 'Client was not found' });
    }
    const client = await Client.deleteOne({ _id });
    return res.status(200).send({ response: 'ok', payload: client });
  } catch (err) {
    return res
      .status(500)
      .send({ response: 'error', message: err.message });
  }
};

const updateClient = async (req, res) => {
  try {
    const { _id } = req.query;
    const arrClients = await Client.find({ email: req.body.email });
    if (arrClients.length > 1
      || (arrClients.length && arrClients[0]._id.toString() !== _id.toString())) {
      return res.status(208).send({ response: 'ok', message: 'Email is already used' });
    }
    const client = await Client.updateOne({ _id }, { $set: req.body });
    return res.status(200).send({ response: 'ok', payload: client });
  } catch (err) {
    return res
      .status(500)
      .send({ response: 'error', message: err.message });
  }
};

const getPaginatedClients = async (req, res) => {
  try {
    let {
      page, limit,
    } = req.query;
    page = page || 1;
    limit = limit || 10;
    const arrayClients = await Client.find({})
      .sort({ name: -1 })
      .skip((page - 1) * +limit)
      .limit(+limit);
    const totalNum = await Client.find({}).countDocuments();

    for (let i = 0; i < arrayClients.length; i += 1) {
      arrayClients[i].providers = await Provider.find({
        _id: arrayClients[i].providers,
      });
    }
    return res.status(200).send({ response: 'ok', payload: { arrayClients, totalNum } });
  } catch (err) {
    return res
      .status(500)
      .send({ response: 'error', message: err.message });
  }
};

module.exports = {
  createClient,
  deleteClient,
  updateClient,
  getPaginatedClients,
};
