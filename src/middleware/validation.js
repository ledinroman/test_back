const Validator = require('../helpers/validator');

const test = (body, res, next, rule) => {
  Validator(body, rule, {}, (err, status) => {
    if (!status) {
      res.status(400).send({
        response: 'error',
        data: err.errors,
      });
    } else {
      next();
    }
  });
};

const addNewUser = (req, res, next) => {
  const validationRule = {
    name: 'required',
    email: 'required|email',
    phone: 'required|size:11',
  };
  test(req.body, res, next, validationRule);
};

const editUser = (req, res, next) => {
  const validationRule = {
    email: 'email',
    phone: 'size:11',
  };
  test(req.body, res, next, validationRule);
};

module.exports = {
  addNewUser,
  editUser,
};
