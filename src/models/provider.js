const mongoose = require('mongoose');

const { Schema } = mongoose;

const Provider = new Schema(
  {
    id: { type: Number, default: 1 },
    name: { type: String, required: true },
  },
  { versionKey: false },
);

module.exports = mongoose.model('Provider', Provider);
module.exports = Provider;
