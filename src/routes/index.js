const express = require('express');
require('../models/index');

const userRouter = require('./userRouter');
const providerRouter = require('./providerRouter');

const router = express.Router();
router.use('/client', userRouter);
router.use('/provider', providerRouter);

module.exports = router;
