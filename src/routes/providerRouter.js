const express = require('express');
const providerController = require('../controllers/providerContoller');

const router = express.Router();

router.post('/create-provider', providerController.createProvider);
router.delete('/delete-provider', providerController.deleteProvider);
router.put('/update-provider', providerController.updateProvider);
router.get('/get-provider', providerController.getProviders);

module.exports = router;
