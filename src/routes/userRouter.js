const express = require('express');
const userController = require('../controllers/userController');
const validator = require('../middleware/validation');

const router = express.Router();

router.post('/create-client', validator.addNewUser, userController.createClient);
router.delete('/delete-client', userController.deleteClient);
router.put('/update-client', validator.editUser, userController.updateClient);
router.get('/get-paginated', userController.getPaginatedClients);

module.exports = router;
